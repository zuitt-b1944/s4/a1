-- =============== S4 A1 Solution ===============


-- A. Find all artists with letter 'd' in its name
	SELECT * FROM artists WHERE name LIKE "%d%";


-- B. Find all songs that has length of less than 230.
	SELECT * FROM songs WHERE length < 230;


-- C. Join the 'albums' and 'songs' tables. (Only show the album name, song name, and song length)
	SELECT album_title, song_name, length FROM artists
		JOIN albums ON artists.id = albums.artist_id
		JOIN songs on albums.id = songs.album_id;


-- D. Join the 'artists' and 'albums' tables (Find all albums that has letter 'a' in its name)
	SELECT * FROM artists 
		JOIN albums ON artists.id = albums.artist_id
		WHERE album_title LIKE "%a%";


-- E. Sort the albums in Z-A order (Show only the first 4 records)
	SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;


-- F. Join the 'albums' and 'songs' tables (Sort albums from Z-A OR sort songs from A-Z)
	SELECT * FROM albums 
		JOIN songs ON albums.id = songs.album_id
		ORDER BY song_name ASC;
		